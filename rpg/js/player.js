let player;
let calcBaseDamage;

function Player(classType, health, mana, strength, agility, defense, speed) {
    this.classType = classType;
    this.health = health;
    this.mana = mana;
    this.strength = strength;
    this.agility = agility;
    this.defense = defense;
    this.speed = speed;
}

let playerMoves = {
    calcAttack: function () {
        let getPlayerSpeed = player.speed;
        let getEnemySpeed = enemy.speed;

        let playerAttack = function () {
            calcBaseDamage;
            if (player.mana > 0) {
                calcBaseDamage = player.strength * player.mana / 1000;
            }
            else {
                calcBaseDamage = player.strength * player.agility / 1000;
            }
            let offsetDamage = Math.floor(Math.random() * Math.floor(10));
            let calcOutputDamage = calcBaseDamage + offsetDamage;
            let numberOfHits = Math.floor(Math.random() * Math.floor(player.agility / 10) / 2) + 1;
            let attackValues = [calcOutputDamage, numberOfHits];
            return attackValues;
        }
        let enemyAttack = function () {
            calcBaseDamage;
            if (enemy.mana > 0) {
                calcBaseDamage = enemy.strength * enemy.mana / 1000;
            }
            else {
                calcBaseDamage = enemy.strength * enemy.agility / 1000;
            }
            let offsetDamage = Math.floor(Math.random() * Math.floor(10));
            let calcOutputDamage = calcBaseDamage + offsetDamage;
            let numberOfHits = Math.floor(Math.random() * Math.floor(enemy.agility / 10) / 2) + 1;
            let attackValues = [calcOutputDamage, numberOfHits];
            return attackValues;
        }
        let getPlayerHealth = document.querySelector(".health-player");

        let getEnemyHealth = document.querySelector(".health-enemy");

        if (getPlayerSpeed >= getEnemySpeed) {
            let playerAttackValues = playerAttack();
            let totalDamage = playerAttackValues[0] * playerAttackValues[1];
            enemy.health = enemy.health - totalDamage;
            alert("You hit the enemy for  " + playerAttackValues[0] + " points of damage " + playerAttackValues[1] + " times, for a total of " + totalDamage);
            if (enemy.health <= 0) {
                alert("You win! press the back button to go back");
                getPlayerHealth.innerHTML = 'Health: 0' + player.health;
                getEnemyHealth.innerHTML = 'Health: 0';
            }
            else {
                getEnemyHealth.innerHTML = 'Health: 0' + enemy.health;
                let enemyAttackValues = enemyAttack();

                let totalDamage = enemyAttackValues[0] * enemyAttackValues[1];
                player.health = player.health - totalDamage;
                alert("The enemy did " + enemyAttackValues[0] + " points of damage " + enemyAttackValues[1] + " times, for a total of " + totalDamage);
                if (player.health <= 0) {
                    alert("You lost! press the back button to go back");
                    getPlayerHealth.innerHTML = 'Health: 0';
                    getEnemyHealth.innerHTML = 'Health: 0' + enemy.health;
                }
                else {
                    getPlayerHealth.innerHTML = 'Health: ' + player.health;
                }
            }
        }
    },
    calcDefence: function() {
        let getPlayerDefence = player.defense;
        let getEnemyStrength = enemy.strength;

        let playerDefence = function () {
            calcBaseDamage;
            if (player.defense <= enemy.strength) {
                calcBaseDamage = enemy.strength * player.defense / 1000;
            }
            else {
                calcBaseDamage = 0;
            }
            let offsetDamage = Math.floor(Math.random() * Math.floor(2));
            let calcOutputDamage = calcBaseDamage * offsetDamage;
            let numberOfHits = Math.floor(Math.random() * Math.floor(enemy.strength / 10) / 2) + 1;
            let attackValues = [calcOutputDamage, numberOfHits];
            return attackValues;
        }

        let getPlayerHealth = document.querySelector(".health-player");


        if (getPlayerDefence >= getEnemyStrength) {
            let playerDefenceValues = playerDefence();
            let totalDamage = playerDefenceValues[0] * playerDefenceValues[1];
            player.health = player.health - totalDamage;

            if (totalDamage <= 0) {
                alert("You succesfully blocked the attack");
                getPlayerHealth.innerHTML = 'Health: 0' + player.health;
                getEnemyHealth.innerHTML = 'Health: 0';
            }
            else {
                getEnemyHealth.innerHTML = 'Health: 0' + enemy.health;
                let enemyAttackValues = enemyAttack();

                let totalDamage = enemyAttackValues[0] * enemyAttackValues[1];
                player.health = player.health - totalDamage;
                alert("The enemy did " + enemyAttackValues[0] + " points of damage " + enemyAttackValues[1] + " times, for a total of " + totalDamage);
                if (player.health <= 0) {
                    alert("You lost! press the back button to go back");
                    getPlayerHealth.innerHTML = 'Health: 0';
                    getEnemyHealth.innerHTML = 'Health: 0' + enemy.health;
                }
                else {
                    getPlayerHealth.innerHTML = 'Health: ' + player.health;
                }
            }
        }
    }
}
