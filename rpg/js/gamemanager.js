let GameManager = {
    setGameStart: function (classType) {
        this.resetPlayer(classType);
        this.setPreFight();
    },
    resetPlayer: function (classType) {
        switch (classType) {
            case "Anass":
                player = new Player(classType, 300, 50, 150, 300, 5000, 65);
                break;
            case "Peter":
                player = new Player(classType, 350, 60, 50, 606, 200, 100);
                break;
            case "Ian":
                player = new Player(classType, 100, 150, 220, 500, 200, 65);
                break;
            case "Kevin":
                player = new Player(classType, 500, 560, 150, 200, 300, 165);
                break;
        }
        let getInterface = document.querySelector(".interface");
        getInterface.innerHTML = '<img src="img/avatars/' + 
        classType.toLowerCase() +'.jpg" class="img-avatar"><div><h3>' + classType 
        + '</h3><p class="health-player">Health: ' + player.health + '</p><p>Mana: ' + player.mana + 
        '</p><p>Strength: ' + player.strength + '</p><p>Agility: ' + player.agility + 
        '</p><p>Defense: ' + player.defense + '</p><p>Speed: ' + player.speed + '</p></div>'

        ;
    },
    setPreFight: function () {
        let getHeader = document.querySelector(".Header");
        let getActions = document.querySelector(".Actions");
        let getArena = document.querySelector(".arena");
        getHeader.innerHTML = '<p> Look for an enemy!</p>';
        getActions.innerHTML = '<button class="btn-prefight" onclick="GameManager.setFight()">Look around for the enemies!</button>';
        getArena.style.visibility = "visible";
    },
    setFight: function(){
        let getHeader = document.querySelector(".Header");
        let getActions = document.querySelector(".Actions");
        let getEnemy = document.querySelector(".enemy");
        let getBg = document.querySelector("bg");
        // Create enemy
        let enemy00 = new Enemy("Don", 300, 100, 100, 300, 200, 50);
        let enemy01 = new Enemy("Chris", 200, 300, 200, 50, 100, 50);
        let enemy02 = new Enemy("Evert", 230, 50, 30, 100, 200, 55);
        let enemy03 = new Enemy("Geert", 300, 150, 120, 100, 100, 50);
        
        let chooseRandomEnemy = Math.floor(Math.random() * Math.floor(4));
        console.log(chooseRandomEnemy);

        switch(chooseRandomEnemy){
            case 0:
                enemy = enemy00;
                break;
            case 1:
                enemy = enemy01
                break;
            case 2:
                enemy = enemy02;
                break;
            case 3: 
            enemy = enemy03;
            break;
        }
        getHeader.innerHTML = '<button value="Refresh Page" onClick="window.location.reload();">Go back</button><p>Choose your move!</p>';
        getActions.innerHTML = '<button class="btn-prefight" onclick="playerMoves.calcAttack()">Attack!</button><button class="btn-prefight" onclick="playerMoves.calcDefence()">Block!</button>';
        getEnemy.innerHTML = '<img src="img/enemy/' + 
        enemy.enemyType.toLowerCase() + '.jpg" alt="' + enemy.enemyType + 
        '"class="img-avatar"><div><h3>' + enemy.enemyType + 
        '</h3><p class="health-enemy">Health: ' + enemy.health + '</p><p>Mana: ' + enemy.mana +
        '</p><p>Strenght:' + enemy.strength + '</p><p> Agility:' + enemy.agility + 
        '</p><p> Defense: ' + enemy.defense + '</p><p> Speed":' + enemy.speed + '</p></div>';
        getBg.innerHTML = "<div <class='back'></div>"

    }
}