let enemy;

function Enemy(enemyType, health, mana, strength, agility, defense, speed){
    this.enemyType = enemyType;
    this.health = health;
    this.mana = mana;
    this.strength = strength;
    this.agility = agility;
    this.defense = defense;
    this.speed = speed;
}